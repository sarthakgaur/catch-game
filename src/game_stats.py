class Stats():
    """A model of game statistics."""

    def __init__(self):
        self.game_active = True
        self.catch_fails_allowed = 3
